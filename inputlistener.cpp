#include "inputlistener.h"

#include <QGuiApplication>


InputListener* InputListener::inst = nullptr;


QObject* InputListener::instance(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    return reinterpret_cast<QObject*>(instance(scriptEngine));
}


InputListener* InputListener::instance(QObject *parent)
{
    if(inst == nullptr)
        inst = new InputListener(parent);

    return inst;
}


InputListener::InputListener(QObject* parent)
    : QObject(parent)
{
}


InputListener::~InputListener()
{
}


bool InputListener::eventFilter(QObject *obj, QEvent *ev)
{
    //Q_UNUSED(obj)

    switch (ev->type()) {
    case QEvent::TouchBegin:
    case QEvent::TouchEnd:
    case QEvent::TouchCancel:
    case QEvent::TouchUpdate:
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
    case QEvent::MouseMove:
    case QEvent::Wheel:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::TabletMove:
    case QEvent::TabletPress:
    case QEvent::TabletRelease:
    case QEvent::TabletEnterProximity:
    case QEvent::TabletLeaveProximity:
        emit input();
        return true;
        break;
    default:
        return QObject::eventFilter(obj, ev);
        break;
    }
}


void InputListener::uninstall()
{
    QGuiApplication::instance()->removeEventFilter(this);
}


void InputListener::install()
{
    QGuiApplication::instance()->installEventFilter(this);
}
