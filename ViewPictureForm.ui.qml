import QtQuick 2.9

Item {
    property alias preview: image.source
    property alias back: back
    property alias message: msg.text

    Text {
        id: msg
        color: "#f0657f"
        text: qsTr("error message")
        font.bold: true
        font.pointSize: 14
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    Image {
        id: image
        anchors.fill: parent
        fillMode: Image.PreserveAspectFit
        asynchronous: true
    }

    LogoBar {
        id: lbar
        LogoButton {
            id: back
        }
    }
}
