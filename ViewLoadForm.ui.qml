import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    //property alias progress: progress
    property alias logo: label1

    Item {
        id: logo
        width: parent.width * .7
        height: label1.implicitHeight + label2.implicitHeight + 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Label {
            id: label1
            color: "#55ff7f"
            text: qsTr("Willkommen")
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: parent.top
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 20
        }

        Label {
            id: label2
            color: "#55ff7f"
            text: qsTr("Click screen to start session...")
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            horizontalAlignment: Text.AlignHCenter
        }

        //        ProgressBar {
        //            id: progress
        //            anchors.right: parent.right
        //            anchors.left: parent.left
        //            anchors.bottom: parent.bottom
        //            indeterminate: false
        //        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
