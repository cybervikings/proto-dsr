import QtQuick 2.9
import QtQuick.Controls 2.2

Item {
    property alias back: back
    property alias content: content
    property alias iso_value: iso_value
    property alias iso_auto: iso_auto
    property alias aperture_value: aperture_value
    property alias aperture_auto: aperture_auto
    property alias shutter_value: shutter_value
    property alias shutter_auto: shutter_auto

    Grid {
        id: content
        anchors.left: lbar.right
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.leftMargin: 48
        anchors.margins: 48
        spacing: 10
        columns: 2

        Label {
            id: label5
            text: qsTr("Kamera Modell:")
        }

        TextField {
            property int prefwidth: (text.length === 0) ? 200 : (text.length * font.pixelSize)
            readOnly: true
            text: cam.deviceId
            width: prefwidth
        }

        Label {
            id: label
            text: qsTr("ISO-Empfindlichkeit")
        }

        Switch {
            id: iso_auto
            text: checked ? "Automatik" : "Manuell"
            checked: settings.isoAuto
        }

        Label {
            id: label1
            text: qsTr("manueller Wert")
        }

        Slider {
            id: iso_value
            from: 100
            stepSize: 100
            snapMode: Slider.SnapAlways
            to: 6400
            enabled: !iso_auto.checked
            value: settings.iso
        }

        Label {
            id: label2
            text: qsTr("angenommener Wert")
        }

        TextField {
            readOnly: true
            text: cam.exposure.iso
        }

        Label {
            text: qsTr("Blendenöffnung")
        }

        Switch {
            id: aperture_auto
            text: checked ? "Automatik" : "Manuell"
            checked: cam.exposure.aperture === -1
        }

        Label {
            text: qsTr("manueller Wert")
        }

        Slider {
            id: aperture_value
            from: 5
            stepSize: 1
            snapMode: Slider.SnapAlways
            to: 32
            enabled: !aperture_auto.checked
        }

        Label {
            text: qsTr("angenommener Wert")
        }

        TextField {
            readOnly: true
            text: cam.exposure.aperture
        }

        Label {
            text: qsTr("Belichtungszeit")
        }

        Switch {
            id: shutter_auto
            text: checked ? "Automatik" : "Manuell"
            checked: cam.exposure.shutterSpeed === -1
        }

        Label {
            text: qsTr("manueller Wert")
        }

        Slider {
            id: shutter_value
            from: 1/200
            stepSize: 1
            snapMode: Slider.SnapAlways
            to: 30
            enabled: !shutter_auto.checked
        }

        Label {
            text: qsTr("angenommener Wert")
        }

        TextField {
            readOnly: true
            text: cam.exposure.shutterSpeed
        }
    }

    LogoBar {
        id: lbar
        LogoButton {
            id: back
            text: "⇦"
        }
    }
}
