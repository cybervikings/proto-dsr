#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "inputlistener.h"


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // App specific variables
    app.setApplicationName("proto-dsr");
    app.setApplicationDisplayName("DSR Prototyp");
    app.setApplicationVersion("0.2");
    app.setOrganizationDomain("cyber-viking.com");
    app.setObjectName("CyberViking Softwareschmiede GbR");

    // register singleton type
    qmlRegisterSingletonType<InputListener>("com.cyberviking", 1, 0, "InputListener", &InputListener::instance);

    // run qml engine and load qml stuff
    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
