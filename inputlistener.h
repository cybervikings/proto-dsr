#ifndef INPUTLISTENER_H
#define INPUTLISTENER_H

#include <QEvent>
#include <QObject>
#include <QQmlEngine>
#include <QJSEngine>

class InputListener : public QObject
{
    Q_OBJECT
private:
    static InputListener* inst;

    InputListener(QObject* parent = 0);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);

public:
    virtual ~InputListener();
    static QObject* instance(QQmlEngine *engine, QJSEngine *scriptEngine);
    static InputListener* instance(QObject* parent = 0);

signals:
    void input();

public slots:
    void uninstall();
    void install();
};

#endif // INPUTLISTENER_H
