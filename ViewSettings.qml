import QtQuick 2.9
import QtQuick.Controls 2.2

ViewSettingsForm {
    id: control
    iso_auto.onCheckedChanged: settings.isoAuto = iso_auto.checked
    iso_value.onValueChanged: settings.iso = iso_value.value
    property real aperture: cam.exposure.manualAperture === -1 ? aperture_value.value : cam.exposure.manualAperture
    property real shutter: cam.exposure.manualShutterSpeed === -1 ? shutter_value.value : cam.exposure.manualShutterSpeed

    aperture_auto.onCheckedChanged: {
        if(aperture_auto.checked) {
            aperture = cam.exposure.manualAperture === -1 ? aperture_value.value : cam.exposure.manualAperture
            cam.exposure.setAutoAperture()
        } else {
            cam.exposure.manualAperture = aperture
        }
    }

    aperture_value.onValueChanged: cam.exposure.manualAperture = aperture_value.value

    shutter_auto.onCheckedChanged: {
        if(shutter_auto.checked) {
            shutter = cam.exposure.manualShutterSpeed === -1 ? shutter_value.value : cam.exposure.manualShutterSpeed
            cam.exposure.setAutoShutterSpeed()
        } else {
            cam.exposure.manualShutterSpeed = shutter
        }
    }

    shutter_value.onValueChanged: cam.exposure.manualShutterSpeed = shutter_value.value

    Connections {
        target: back
        onClicked: views.pop()
    }
}
