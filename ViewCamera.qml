import QtQuick 2.9
import QtQuick.Controls 2.2
import QtMultimedia 5.9

ViewCameraForm {
    StackView.onActivating: cam.start()
    StackView.onDeactivating: cam.stop()
    camview.source: cam
    shutdown.onClicked: app.close()

    Connections {
        target: cam.flash
        onFlashModeChanged: console.log("Flash mode:", cam.flash.mode)
    }

    Connections {
        target: cam.imageCapture
        onImageCaptured: views.push(view_pic, {"preview" : preview})
        onCaptureFailed: views.push(view_pic, {"message" : message})
    }

    Connections {
        target: lock
        onClicked: cam.searchAndLock()
        onReleased: cam.unlock()
    }

    Connections {
        target: shoot
        onClicked: cam.searchAndLock()
        onReleased: cam.imageCapture.capture()
    }

    Connections {
        target: play
        onClicked: cam.cameraState === Camera.ActiveState ? cam.stop( ) : cam.start()
    }

    Connections {
        target: config
        onClicked: views.push(view_cfg)
    }

    Connections {
        target: exit
        onClicked: views.push(view_load)
    }
}
