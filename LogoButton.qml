import QtQuick 2.9
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

ToolButton {
    property color color: "#55ff7f"

    id: tbtn
    width: Math.min(parent.width, parent.height)
    height: width
    hoverEnabled: true
    background: null
    font.pixelSize: height

    onHoveredChanged: hovered ? heffect.brightness = 0.5 : heffect.brightness = 0

    contentItem: Text {
        id: lbl
        padding: 0
        text: tbtn.text
        fontSizeMode: Text.Fit
        font: tbtn.font
        color: enabled ? tbtn.color : "grey"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: false
            horizontalOffset: 0
            verticalOffset: 0
            radius: 8
        }
    }

    BrightnessContrast {
        id: heffect
        anchors.fill: lbl
        source: lbl
        brightness: 0

        Behavior on brightness {
            NumberAnimation {duration: 100}
        }
    }
}
