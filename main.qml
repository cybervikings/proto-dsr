import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtMultimedia 5.9

ApplicationWindow {
    id: app
    visible: true
    width: 640
    height: 480
    background: Rectangle {
        color: "black"
    }

    Component {id: view_load; ViewLoad {}}
    Component {id: view_cam; ViewCamera {}}
    Component {id: view_pic; ViewPicture {}}
    Component {id: view_cfg; ViewSettings{}}

    Camera {
        id: cam
        cameraState: Camera.LoadedState
        exposure {
            //manualIso: settings.iso
            exposureMode: settings.exposureMode
        }

        Component.onCompleted: {
            settings.isoAuto ? cam.exposure.setAutoIsoSensitivity() : cam.exposure.manualIso = settings.iso
        }
        onCameraStateChanged: console.log("II switched to state: ", cam.cameraState)
    }

    Settings {
        id: settings
        category: "app_camera"

        property int iso: cam.exposure.iso
        property bool isoAuto: (cam.exposure.manalIso === -1)
        property int exposureMode: cam.exposure.exposureMode
    }

    Connections {
        target: settings
        onIsoAutoChanged: (settings.isoAuto) ? cam.exposure.setAutoIsoSensitivity() : cam.exposure.manualIso = settings.iso
    }

    Binding {
        target: cam.exposure
        property: "manualIso"
        value: settings.iso
        when: !settings.isoAuto
    }

    StackView {
        id: views
        anchors.fill: parent
        initialItem: view_load
        pushEnter: fadeIn
        pushExit: fadeOut
        popEnter: fadeIn
        popExit: fadeOut

        property int duration: 200

        Transition {
            id: fadeIn
            PropertyAnimation {
                property: "opacity"
                from: 0
                to: 1
                duration: views.duration
            }
        }

        Transition {
            id: fadeOut
            PropertyAnimation {
                property: "opacity"
                from: 1
                to: 0
                duration: views.duration
            }
        }
    }
}
