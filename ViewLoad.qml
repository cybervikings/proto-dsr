import QtQuick 2.9
import QtMultimedia 5.9
import QtQuick.Controls 2.2
import com.cyberviking 1.0

ViewLoadForm {
    id: root
    focus: true
    logo.text: "📷"

    StackView.onActivated: InputListener.install()
    StackView.onDeactivating: InputListener.uninstall()

    RotationAnimation on logo.rotation {
        from: 0;
        to: 360;
        duration: 3000
        running: true
        loops: Animation.Infinite
    }

    Connections {
        target: InputListener
        onInput: views.push(view_cam)
    }

//    progress.from: Camera.UnloadedStatus
//    progress.to: Camera.ActiveStatus
//    progress.value: v4lcam.cameraStatus

//    Behavior on progress.value {
//        NumberAnimation{}
//    }

//    Connections {
//        target: v4lcam
//        onCameraStateChanged: {if (state == Camera.ActiveState) {
//                //label.text = "Fertig"
//                views.push(view_cam)
//            }
//        }
//    }
}
