import QtQuick 2.9
import QtMultimedia 5.9
import QtQuick.Controls 2.2

Item {
    id: item1
    property alias camview: camview
    property alias shoot: shoot
    property alias play: play
    property alias config: config
    property alias exit: exit
    property alias lock: lock
    property alias shutdown: shutdown

    // andere symbole: ⇨ ⎙ 📷 ⌨ ✕ 🗙 ❌ ⟲ ☠ ⚠ ✉ ✎ ✍ ✒ ➢ ⁝ ⁕ ⁜ ⚡ 🜊 ☩ ☀ 🌗 ⏻ ⏹ ⏾ ⏣

    VideoOutput {
        id: camview
        anchors.fill: parent
        focus: visible
        visible: true
    }

    LogoBar {
        LogoButton {
            id: play
            text: cam.cameraState === Camera.ActiveState ? "⏸" : "⏵"
        }

        LogoButton {
            id: shoot
            text: "📷"
        }

        LogoButton {
            id: lock
            text: "☩"
        }

        LogoButton {
            id: config
            text: "🛠"
        }

        LogoButton {
            id: exit
            text: "⏾"
        }

        LogoButton {
            id: shutdown
            text: "⏻"
        }
    }
}
