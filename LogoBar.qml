import QtQuick 2.9
import QtQuick.Controls 2.2

ToolBar {
    default property alias children: menu.children

    id: lb
    width: 64
    spacing: 0
    anchors.left: parent.left
    anchors.leftMargin: 32
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 32
    anchors.top: parent.top
    anchors.topMargin: 32
    background: null

    Column {
        id: menu
        spacing: lb.spacing
        anchors.fill: parent
    }
}
